#include <SoftwareSerial.h>

SoftwareSerial miBT(10,11); // Pin BlueTooth

char DATO;
int minutos;
int segundos;
char DATOcorte;
int LEDROJO = 5;
int LEDAZUL = 3;
int RELAY = 2; 


void setup() {
  Serial.begin(9600);
  Serial.println("Serial Begin");
  miBT.begin(38400); // configuro velocidad de comunicación Bluetooh

 /*
  // IMPORTNTE >> Para modulos blootooh sin boton tenemos que utilizar el Wakeup -> sino comentar el siguiente parrafo:
    Serial.println("Iniciando Bluetooth...");
    pinMode(6,OUTPUT); // VCC Bluetooh
    pinMode(7,OUTPUT); // Wakeup Bluetooh que reemplaza el botón si no esta
    digitalWrite(7,HIGH); // Simulamos apretar el botón
    digitalWrite(6,HIGH); // Encendemos el Bluetooh
    delay(5000); // Esperamos 5 segundos
    digitalWrite(7,LOW); // Simulamos soltar botón
    Serial.println("Bluetooth iniciado");
  // -------------------------------------------------------
  */

  pinMode(LEDROJO, OUTPUT);
  pinMode(LEDAZUL, OUTPUT);
  pinMode(RELAY, OUTPUT);
  
  digitalWrite(RELAY,HIGH); // Mantenemos apagado el relay (logica invertida, se apaga alimentandolo el pin IN del Relay)
  digitalWrite(LEDROJO,HIGH);
  digitalWrite(LEDAZUL,LOW); 

// FIN SETUP
}




/*  FUNCIONES */ 

// ***** FUNCION DE RESET --> PARA REINICIAR EL PROGRAMA SI LO CREO CONVENIENTE ANTE CIERTAS CIRCUNSTANCIAS
       void(* resetFunc) (void) = 0;//declare reset function at address 0




void loop() {
  
  if(miBT.available()){  // Si hay datos del bluetooth 

    digitalWrite(LEDAZUL,HIGH); //Encendemos led Azul en caso de estar conectados por Bluetooth
    
    // LEEMOS EL DATO ENVIADO POR BLUETOOTH
      DATO = miBT.read();
      Serial.write(DATO);

    // ----> APAGAMOS RELAY
      if(DATO == 'b'){
        digitalWrite(RELAY,HIGH);
        Serial.println("RELAY OFF");

        digitalWrite(LEDAZUL,LOW); 
      }

    // ----> ENCENDEMOS RELAY 
      if(DATO == 'a'){
        digitalWrite(RELAY,LOW);
        Serial.println("RELAY ON");

        digitalWrite(LEDAZUL,HIGH);
      }

    // ----> TEMPORIZADOR 
    if(DATO != 'a' && DATO != 'b'){

      // REINICIAMOS LAS VARIABLES YA QUE SON GLOBALES
     
      minutos = 0;
      segundos = 0;
      
      switch (DATO) {
          case '1':
            minutos = 30;
            break;
          case '2':
            minutos = 45;
            break;
          case '3':
            minutos = 60;
            break;
          case '4':
            minutos = 90;
            break;
          case '5':
            minutos = 120;
            break;
          case '6':
            minutos = 160;
            break;
          case '7':
            minutos = 190;
            break;
          case '8':
            minutos = 220;
            break;  

                      
          default:
            resetFunc();
            break;
        }


      
      // Serial.println(DATO);
      Serial.print("INICIAR TEMPORIZADOR: ");
      Serial.print(minutos);
      Serial.println(" Minutos");
      digitalWrite(RELAY,LOW);      // con LOW ENCENDEMOS RELAY 
      Serial.println("RELAY ON");
              
        
         segundos = 60*minutos;
        
        for(int i=0 ; i<= segundos ; i++){
          
          digitalWrite(LEDAZUL,HIGH);
          Serial.print("Timmer:");
          Serial.println(i);
          delay(500);
          digitalWrite(LEDAZUL,LOW);
          delay(500);
          
          // Volvemos a leer bluetooth
          DATOcorte = miBT.read();
          Serial.write(DATOcorte);
          
          // Si nos entrega 0 o el contador i llego al maximo apagamos relay y reseteamos programa
          if(DATOcorte == 'b' || i >= segundos){
            digitalWrite(RELAY,HIGH);
            Serial.write("RELAY OFF");
            Serial.println("FIN TIMMER");
            resetFunc(); // RESET 
          }
        }
     }

    
    /*  
      Serial.write(miBT.read()); // Escribimos en consola lo que leemos   
      }
    
      if(Serial.available()){ // Si escribimos en el serial
        miBT.write(Serial.read()); // El Bluetooh lo retransmite como comando
      }
    */
  } else { // En caso que no este conectado el Bluetooth parpadeamos el Led Azul  

        /*
        digitalWrite(LEDAZUL,HIGH);
        delay(500);
        digitalWrite(LEDAZUL,LOW);
        delay(500);
        digitalWrite(LEDAZUL,HIGH);
        delay(500);
        digitalWrite(LEDAZUL,LOW);
        */    
  }
    

}
