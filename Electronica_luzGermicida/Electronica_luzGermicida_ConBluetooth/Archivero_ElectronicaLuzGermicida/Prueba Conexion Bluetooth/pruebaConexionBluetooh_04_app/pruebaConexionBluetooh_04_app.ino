#include <SoftwareSerial.h>

SoftwareSerial miBT(10,11); // Pin BlueTooth

char DATO;
//int DATO;
char DATOcorte;
int LEDROJO = 5;
int LEDAZUL = 3;
int RELAY = 2; 


void setup() {
  Serial.begin(9600);
  Serial.println("Serial Begin");
  miBT.begin(38400); // configuro velocidad de comunicación Bluetooh

 /*
  // IMPORTNTE >> Para modulos blootooh sin boton tenemos que utilizar el Wakeup -> sino comentar el siguiente parrafo:
    Serial.println("Iniciando Bluetooth...");
    pinMode(6,OUTPUT); // VCC Bluetooh
    pinMode(7,OUTPUT); // Wakeup Bluetooh que reemplaza el botón si no esta
    digitalWrite(7,HIGH); // Simulamos apretar el botón
    digitalWrite(6,HIGH); // Encendemos el Bluetooh
    delay(5000); // Esperamos 5 segundos
    digitalWrite(7,LOW); // Simulamos soltar botón
    Serial.println("Bluetooth iniciado");
  // -------------------------------------------------------
  */

  pinMode(LEDROJO, OUTPUT);
  pinMode(LEDAZUL, OUTPUT);
  pinMode(RELAY, OUTPUT);
  
  digitalWrite(RELAY,HIGH); // Mantenemos apagado el relay (logica invertida, se apaga alimentandolo el pin IN del Relay)
  digitalWrite(LEDROJO,HIGH);
  digitalWrite(LEDAZUL,LOW); 

// FIN SETUP
}




/*  FUNCIONES */ 

// ***** FUNCION DE RESET --> PARA REINICIAR EL PROGRAMA SI LO CREO CONVENIENTE ANTE CIERTAS CIRCUNSTANCIAS
       void(* resetFunc) (void) = 0;//declare reset function at address 0




void loop() {
  
  if(miBT.available()){  // Si hay datos del bluetooth 

    digitalWrite(LEDAZUL,HIGH); //Encendemos led Azul en caso de estar conectados por Bluetooth
    
    // LEEMOS EL DATO ENVIADO POR BLUETOOTH
      DATO = miBT.read();
      Serial.write(DATO);

    // ----> APAGAMOS RELAY
      if(DATO == '0'){
        digitalWrite(RELAY,HIGH);
        Serial.write("RELAY OFF");

        digitalWrite(LEDAZUL,LOW); 
      }

    // ----> ENCENDEMOS RELAY 
      if(DATO == '1'){
        digitalWrite(RELAY,LOW);
        Serial.write("RELAY ON");

        digitalWrite(LEDAZUL,HIGH);
      }

    // ----> ENCENDEMOS RELAY POR 30 MIN 
    if(DATO == '2'){
      Serial.println("ENCENDEMOS RELAY POR 30 MIN");
      digitalWrite(RELAY,LOW);
      Serial.write("RELAY ON");
      
      // hacemos que dure 30 min chequeado el serial por si quieren interrumpir
        int TreintaMin = 60*30;
        
        for(int i=0 ; i<= TreintaMin ; i++){
          
          digitalWrite(LEDAZUL,HIGH);
          Serial.print("Timmer:");
          Serial.println(i);
          delay(500);
          digitalWrite(LEDAZUL,LOW);
          delay(500);
          
          // Volvemos a leer bluetooth
          DATOcorte = miBT.read();
          Serial.write(DATOcorte);
          
          // Si nos entrega 0 o el contador i llego al maximo apagamos relay y reseteamos programa
          if(DATOcorte == '0' || i >= TreintaMin){
            digitalWrite(RELAY,HIGH);
            Serial.write("RELAY OFF");
            Serial.println("FIN TIMMER");
            resetFunc(); // RESET 
          }
        }
     }

    
    /*  
      Serial.write(miBT.read()); // Escribimos en consola lo que leemos   
      }
    
      if(Serial.available()){ // Si escribimos en el serial
        miBT.write(Serial.read()); // El Bluetooh lo retransmite como comando
      }
    */
  } else { // En caso que no este conectado el Bluetooth parpadeamos el Led Azul  

        /*
        digitalWrite(LEDAZUL,HIGH);
        delay(500);
        digitalWrite(LEDAZUL,LOW);
        delay(500);
        digitalWrite(LEDAZUL,HIGH);
        delay(500);
        digitalWrite(LEDAZUL,LOW);
        */    
  }
    

}
