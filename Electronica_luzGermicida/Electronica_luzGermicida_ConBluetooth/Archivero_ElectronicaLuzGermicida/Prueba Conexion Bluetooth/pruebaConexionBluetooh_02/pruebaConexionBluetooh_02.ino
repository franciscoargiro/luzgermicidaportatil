#include <SoftwareSerial.h>

SoftwareSerial miBT(10,11);


void setup() {
  Serial.begin(9600);
  Serial.println("Serial Begin");
  miBT.begin(38400); // configuro velocidad de comunicación Bluetooh

  // IMPORTNTE >> Para modulos blootooh sin boton tenemos que utilizar el Wakeup -> sino comentar el siguiente parrafo:
    Serial.println("Iniciando Bluetooth...");
    pinMode(6,OUTPUT); // VCC Bluetooh
    pinMode(7,OUTPUT); // Wakeup Bluetooh que reemplaza el botón si no esta
    digitalWrite(7,HIGH); // Simulamos apretar el botón
    digitalWrite(6,HIGH); // Encendemos el Bluetooh
    delay(5000); // Esperamos 5 segundos
    digitalWrite(7,LOW); // Simulamos soltar botón
    Serial.println("Bluetooth iniciado");
  // -------------------------------------------------------

}

void loop() {
  
  if(miBT.available()){  // Si hay datos del bluetooth 
   Serial.write(miBT.read()); // Escribimos en consola lo que leemos   
  }

  if(Serial.available()){ // Si escribimos en el serial
    miBT.write(Serial.read()); // El Bluetooh lo retransmite como comando
  }
  
}
