#include <SoftwareSerial.h>

// VERSION 1.4 -> ENCENDIDO EN AUTOMATICO A LOS 15 SEGUNDOS DEL SWICH ON Y LUEGO PERMANECE LA LUZ PRENDIDA DURANTE 15 MINUTOS

int LEDROJO = 5;
int LEDAZUL = 3;
int RELAY = 2; 


void setup() {
  
  Serial.begin(9600);
  Serial.println("Serial Begin");
  

  pinMode(LEDROJO, OUTPUT);
  pinMode(LEDAZUL, OUTPUT);
  pinMode(RELAY, OUTPUT);
  
  digitalWrite(RELAY,HIGH); // RELAY OFF -> Mantenemos apagado el relay (logica invertida, se apaga alimentandolo el pin IN del Relay)
  digitalWrite(LEDROJO,HIGH);
  digitalWrite(LEDAZUL,LOW);

  int i=0;
  Serial.println("Iniciamos While de 15 segundos");
  while(i<15){
    digitalWrite(LEDAZUL,HIGH);
    delay(500);
    digitalWrite(LEDAZUL,LOW);
    delay(500);
    Serial.println(i);
    i++;  
  }

  Serial.println("Relay ON durante 15 min");
  digitalWrite(LEDAZUL,HIGH);
  digitalWrite(RELAY,LOW); // RELAY ON 
  delay(900000); // 15 minutos

  Serial.println("Relay OFF");
  digitalWrite(LEDAZUL,LOW);
  digitalWrite(RELAY,HIGH); // RELAY OFF  

// FIN SETUP
}



void loop(){
  
}
